import { Body, Controller, Get, Param, Post, Put } from '@nestjs/common';
import { ProductService } from './product.service';

@Controller('product')
export class ProductController {
  constructor(private productService: ProductService) {}
  @Get(':catagory/:catagoryType/:brand/:brandType')
  async getProducts(
    @Param('catagory') catagory: string,
    @Param('catagoryType') catagoryType: string,
    @Param('brand') brand: string,
    @Param('brandType') brandType: string,
  ) {
    return await this.productService.getAll(
      catagory,
      catagoryType,
      brand,
      brandType,
    );
  }
  @Get('allproducts')
  async getAllProducts() {
    return await this.productService.getAllProducts();
  }
  @Get('newproducts')
  async getNewProducts() {
    return await this.productService.getAllNewProducts();
  }
  @Post()
  async createProduct(@Body() body): Promise<string> {
    return await this.productService.creat(body);
  }
  @Put('/inStock/:productId')
  async updateProduct(@Param('productId') productId: string) {
    return await this.productService.updateInStock(productId);
  }
}
