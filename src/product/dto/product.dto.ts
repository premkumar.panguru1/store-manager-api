/* eslint-disable prettier/prettier */
export class ProductDto {
  catagory: string;
  catagoryType: string;
  brand: string;
  brandType: string;
  name: string;
  minRange: number;
  maxRange: number;
  description: string;
  inStock: boolean;
}
