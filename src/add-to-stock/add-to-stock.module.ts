import { Module } from '@nestjs/common';
import { AddToStockService } from './add-to-stock.service';
import { NewStockController } from './add-to-stock.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { Stock, StockSchema } from './schema/add-to-stock.schema';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Stock.name, schema: StockSchema }]),
  ],
  providers: [AddToStockService],
  exports: [AddToStockService],
  controllers: [NewStockController],
})
export class AddToStockModule {}
