export class NewStockDto {
  preoductId: string;
  productName: string;
  barcode: string;
  quantity: number;
  indexRange: {
    from: number;
    to: number;
  };
  price: {
    selling: number;
    buying: number;
  };
  description: string;
}
