import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { AddToStockService } from './add-to-stock.service';

@Controller('stock')
export class NewStockController {
  constructor(private addToStockService: AddToStockService) {}
  @Get(':productId')
  async getStock(@Param('productId') productId: string) {
    const stock = await this.addToStockService.getStock(productId);
    return await stock;
  }
  @Post()
  async createNewStock(@Body() body): Promise<string> {
    return await this.addToStockService.createStock(body);
  }
}
