import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { NewStockDto } from './dto/add-to-stock.dto';
import { Stock, StockDocument } from './schema/add-to-stock.schema';

@Injectable()
export class AddToStockService {
  constructor(
    @InjectModel(Stock.name)
    private stockModel: Model<StockDocument>,
  ) {}
  async createStock(newStockDto: NewStockDto) {
    let createdNewStock;
    try {
      createdNewStock = new this.stockModel(newStockDto);
    } catch (error) {
      console.log(error);
    }
    return await createdNewStock.save();
  }
  async getStock(productid) {
    const allStock = await this.stockModel.find({ productId: productid });
    return await allStock;
  }
}
