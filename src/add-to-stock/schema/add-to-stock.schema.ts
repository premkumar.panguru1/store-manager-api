import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type StockDocument = Stock & Document;

@Schema({ timestamps: true })
export class Stock extends Document {
  @Prop({ type: String, required: true })
  productId: string;
  @Prop({ type: String, required: true })
  productName: string;
  @Prop({ type: String, required: true })
  barcode: string;
  @Prop({ type: Number, required: true })
  quantity: number;
  @Prop({ type: { from: Number, to: Number, _id: false }, required: true })
  indexRange: number;
  @Prop({
    type: { buying: Number, selling: Number, _id: false },
    required: true,
  })
  price: number;
  @Prop({ type: String, required: false })
  description: string;
}
export const StockSchema = SchemaFactory.createForClass(Stock);
